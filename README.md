## 动态数据梳理

### Project

- [x] 1 - create，新建项目
- [x] 1 - imported，导入项目
- [ ] 12 - change，更新项目基本信息（含归档、修改 path）
- [ ] 10 - destory，删除项目
- [ ] 4 - http push，http 推送
- [ ] 7 - ssh push，ssh 推送
- [ ] 13 - transfer，转移项目
- [ ] 14 - remove，删除项目
- [ ] 27 - star，Star 项目

### Push

- [ ] 5 - push to，推送、强制推送
- [ ] 5 - push new，新建分支、新建 tag
- [ ] 5 - deleted，删除分支、删除 tag
- [ ] 5 - batch delete branches，批量删除分支

### PR

- [ ] 1 - created，新建 PR
- [ ] 4 - opened， 重新打开 PR
- [ ] 7 - accepted，合入 PR
- [ ] 3 - closed，关闭 PR

### Issue

- [ ] 1 - created，新建 issue
- [ ] 1 - opened，重新打开 issue
- [ ] 3 - cloesed，关闭 issue

### Note

- [ ] 6 - commented on，PR 的 Codereview
- [ ] 6 - commented on，PR 的 评论
- [x] 6 - ~~commented on，PR 的评审意见~~
- [ ] 6 - commented on，issue 评论
- [ ] 6 - commented on，commit 评论

### 里程碑

- [ ] 1 - created，新建里程碑
- [ ] 3 - closed，关闭里程碑
- [ ] 10 - destroyed，删除里程碑

### Release

- [ ] 1 - created，新建发行版
- [ ] 12 - changed，编辑发行版
- [ ] 14 - remove，删除发行版

### Group

- [ ] 1 - created，新建组织
- [ ] 7 - destroyed，删除组织
- [ ] 26 - follow，关注组织

### 其它

- [ ] 8 - joined，加入项目、加入项目
- [ ] 9 - left，退出项目、退出组织
- [ ] 19 - change member，权限角色调整
- [x] 11 - ~~removed due to membership expiration from，有效期结束自动退出~~